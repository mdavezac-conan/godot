from conans import ConanFile, tools


class GodotHeadersrConan(ConanFile):
    name = "GodotHeaders"
    version = "3.1"
    license = "MIT"
    url = "https://gitlab.com/mdavezac-conan/godotheaders"
    homepage = "https://github.com/GodotNativeTools/godot_headers.git"
    description = "Free and open source community-driven 2D and 3D game engine"
    topics = ("Game engine",)
    no_copy_source = True

    def source(self):
        git = tools.Git(folder="godot")
        git.clone(self.homepage, branch="master")

    def package(self):
        self.copy("godot/gdnative/*", dst="include")
        self.copy("godot/nativescript/*", dst="include")
        self.copy("godot/gdnative_api_struct.gen.h", dst="include")

    def package_id(self):
        self.info.header_only()
